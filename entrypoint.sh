#!/bin/sh

export APP_VERSION=$(poetry version --no-ansi | cut -d ' ' -f 2)

if [ "$APP_ENV" = "development" ]; then
  poetry run uvicorn app.main:app --reload --host 0.0.0.0 --port 8004
else
  export PATH=$PATH:/home/devuser/.local/bin
  poetry run uvicorn app.main:app --host 0.0.0.0 --port 8004 --root-path /carbon-emission/v1
fi
