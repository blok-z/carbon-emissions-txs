# from datetime import datetime, timedelta

# import pytest
# from fastapi.encoders import jsonable_encoder

# from app.handlers.ccri import CCRIManager
# from app.schemas.ccri import (EmissionResponse, EntriesInResponse,
#                               MetaDataInResponse)

# """
# pytest tests/test_handlers/test_ccri.py --capture=no
# """

# ccri = CCRIManager()

# expected_result = {
#     "entries": [
#         {
#             "date": "2022-07-01",
#             "entryValue": "1",
#             "outputValue": "8.602886062821824e-09",
#         }
#     ],
#     "metaData": {
#         "sum": 8.602886062821824e-09,
#         "mean": 8.602886062821824e-09,
#         "inputUnit": "gwei",
#         "outputUnit": "t",
#         "description": "Allocated the emissions for Ethereum to txFee with a tx-based allocation strategy, gwei as input and t as output.",
#         "warning": [],
#     },
# }


# @pytest.mark.asyncio
# async def test_handlers_ccri_get_tx_based_pow_result():
#     # def generate_date_list(start_date, n):
#     #     date_list = []
#     #     current_date = datetime.strptime(start_date, settings.DTFORMAT)
#     #     for i in range(n):
#     #         date_list.append(current_date.strftime(settings.DTFORMAT))
#     #         current_date += timedelta(days=1)
#     #     return date_list

#     start_date = "2022-07-01"
#     no_of_dates = 1
#     # date_list = generate_date_list(start_date, no_of_dates)
#     ccri_response = await ccri.get_tx_based_pow_result("eth", date=start_date)
#     assert isinstance(ccri_response, EmissionResponse)
#     assert len(ccri_response.entries) == no_of_dates
#     assert ccri_response.entries[0].date == start_date


# @pytest.mark.asyncio
# async def test_handlers_ccri_get_tx_based_pos_result():
#     # def generate_date_list(start_date, n):
#     #     date_list = []
#     #     current_date = datetime.strptime(start_date, settings.DTFORMAT)
#     #     for i in range(n):
#     #         date_list.append(current_date.strftime(settings.DTFORMAT))
#     #         current_date += timedelta(days=1)
#     #     return date_list

#     start_date = "2023-01-01"
#     no_of_dates = 1
#     # date_list = generate_date_list(start_date, no_of_dates)
#     ccri_response = await ccri.get_tx_based_pos_result("eth", date=start_date)
#     assert isinstance(ccri_response, EmissionResponse)
#     assert len(ccri_response.entries) == no_of_dates
#     assert ccri_response.entries[0].date == start_date


# @pytest.mark.asyncio
# async def test_handlers_ccri_get_coeffient():
#     start_date = "1672608783"
#     no_of_dates = 1
#     coefficient = await ccri.get_coefficient(ts=start_date, chain="eth")
#     assert isinstance(coefficient, float)
#     # assert len(ccri_response.entries) == no_of_dates
#     # assert ccri_response.entries[0].date == start_date
