from fastapi import APIRouter

from app.api.rest import emission
from app.api.ws import address

router = APIRouter()

router.include_router(emission.router, prefix="/db")
router.include_router(address.router, prefix="/ws")
