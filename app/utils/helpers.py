from datetime import datetime

import pytz


def set_timestamp():
    # REF https://docs.sqlalchemy.org/en/20/core/custom_types.html#store-timezone-aware-timestamps-as-timezone-naive-utc
    return datetime.now(tz=pytz.timezone("Europe/Istanbul")).replace(tzinfo=None)
