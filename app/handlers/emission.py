from datetime import datetime

import pandas as pd
from eth_typing.evm import HexAddress

from app.config.settings import settings
from app.handlers.ccri import ccri_manager
from app.schemas.api import AggregatedData, ChainId
from app.schemas.scanner import TransactionResponse


class TransactionManager:
    def __init__(
        self, address: HexAddress, chain_id: ChainId, data: TransactionResponse
    ):
        self.df = pd.DataFrame([o.dict(by_alias=False) for o in data.result])
        self.address = address  # address: HexAddress
        self.chain_id = chain_id

    def set_day_column(self) -> None:
        self.df["day"] = pd.to_datetime(self.df["timestamp"], unit="s").dt.date

    def set_tx_fee_column(self, unit: str = "gwei") -> None:  # TODO add ccri unit model
        # calculate the transaction fee for each row
        # gasPrice unit is WEI
        self.df["tx_fee"] = ((self.df["gas_price"] / 1e9) * self.df["gas_used"]).astype(
            int
        )  # convert to GWEI

    async def set_coefficient_column(self) -> None:
        # # FIXME buggy check ram and rate-limit
        # results = await asyncio.gather(
        #     *[
        #         ccri_manager.get_coefficients(ts, chain_id=self.chain_id)
        #         for ts in self.df["timestamp"]
        #     ]
        # )

        await ccri_manager.set_coefficients(
            chain_id=self.chain_id,
            dates=[
                d.strftime(settings.DTFORMAT)
                for d in pd.to_datetime(self.df["timestamp"], unit="s").dt.date.unique()
            ],
        )
        results = await ccri_manager.get_coefficients(
            chain_id=self.chain_id, timestamps=self.df["timestamp"].to_list()
        )
        self.df["coefficient"] = results

    def calculate_carbon_emission(self, mode: str = "", decimal: int = 3) -> float:
        merge_date = datetime.strptime(
            settings.CCRI__MERGE_DATE, settings.DTFORMAT
        ).date()
        # TODO check values once again. It should return CO2 in KG

        if self.chain_id.value == 1:
            self.df.loc[self.df["day"] < merge_date, "carbon_emission"] = (
                self.df["tx_fee"] * self.df["coefficient"]
            )
            self.df.loc[self.df["day"] >= merge_date, "carbon_emission"] = (
                self.df["gas_used"] * self.df["coefficient"]
            )
            total = self.df["carbon_emission"].sum()
        else:
            self.df["carbon_emission"] = self.df["gas_used"] * self.df["coefficient"]
            total = self.df["carbon_emission"].sum()
        return float(int(total * 10**decimal)) / 10**decimal

    async def aggregated_blocks(self) -> AggregatedData:
        # make CCRI API call for those unique dates if not exist in cache
        self.set_day_column()  # create day column from timestamps
        self.set_tx_fee_column()  # create tx_fee column and calculate from tx_response
        await self.set_coefficient_column()  # create coefficient column from api or cache

        return AggregatedData(
            chain_id=self.chain_id,
            address=self.address,
            block_start=self.df["block_number"].min(),
            block_end=self.df["block_number"].max(),
            timestamp_start=self.df["timestamp"].min(),
            timestamp_end=self.df["timestamp"].max(),
            tx_fee=self.df["tx_fee"].sum(),
            tx_count=len(self.df),
            gas_used=self.df["gas_used"].sum(),
            co2=self.calculate_carbon_emission(),
        )
