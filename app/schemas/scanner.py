from eth_typing.evm import HexAddress
from pydantic import BaseModel, Field

from app.config.settings import settings


class Transaction(BaseModel):
    # hash: HexStr
    # from_address: HexAddress = Field(alias="from")
    # to_address: HexAddress = Field(alias="to")
    block_number: int = Field(alias="blockNumber")
    gas: int = Field(alias="gas")
    gas_price: int = Field(alias="gasPrice")
    gas_used: int = Field(alias="gasUsed")
    timestamp: int = Field(alias="timeStamp")
    is_error: int = Field(alias="isError")

    class Config:
        arbitrary_types_allowed = True
        allow_population_by_field_name = True


class TransactionRequest(BaseModel):
    module: str = "account"
    action: str = "txlist"
    address: HexAddress
    startblock: int
    endblock: int
    page: int = 1
    offset: int = settings.OFFSET
    sort: str = "asc"
    apikey: str


class TransactionResponse(BaseModel):
    status: int
    message: str
    result: list[Transaction] | None


class BlockRequest(BaseModel):
    module: str = "proxy"
    action: str = "eth_blockNumber"  # TODO check if same at all providers
    apikey: str
