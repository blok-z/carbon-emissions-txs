from enum import Enum, IntEnum

from eth_typing.evm import HexAddress
from pydantic import BaseModel, Field

# Websocket Service


class ChainId(IntEnum):
    ETH = 1
    MATIC = 137
    BNB = 56
    CELO = 42220


class InComingData(BaseModel):
    chain_id: ChainId
    addresses: list[HexAddress]
    block_start: int = Field(default=0)
    block_end: int = Field(default=0)

    # FIXME add some validations
    # @validator("block_end")
    # def check_block_end(cls, value, values):
    #     if "block_start" in values and value <= values["block_start"]:
    #         raise ValueError("block_end must be greater than block_start")
    #     return value

    # @validator("address", pre=True, each_item=True)
    # def parse_hexstr_as_int(cls, v):
    #     return int(v, 16) if isinstance(v, str) else v


class AggregatedData(BaseModel):
    chain_id: ChainId
    address: HexAddress
    block_start: int
    block_end: int
    timestamp_start: int
    timestamp_end: int
    gas_used: int
    tx_fee: int
    co2: float
    tx_count: int


class OutGoingData(BaseModel):
    client_id: str
    uid: HexAddress | None
    status: str = "1"
    message: str = "OK"
    result: AggregatedData | None
