from enum import Enum

from pydantic import BaseModel, Field

"""

Transaction-based (alias tx-based)
Allocation of total carbon footprint based on the share of number of transactions. Alternatively, the share of the transaction fee or the share of transaction size can be utilized. For this approach, it is assumed that transactions are the driver for electricity consumption and corresponding emissions. If this assumption is supposed to hold valid, electricity consumption should fall (close) to zero if no transactions are performed.

Date (alias datehttps://v2.api.ccri.tech)
All dates follow the ISO-8601 format (YYYY-MM-DD), e.g. the 1st February 2021 translates to 2021-02-01

"""


class InputUnit(str, Enum):
    WEI = "wei"
    GWEI = "gwei"
    ETHER = "ether"
    GAS = "gas"
    # other units


class OutputUnit(str, Enum):
    GRAM = "g"
    KG = "kg"
    TON = "t"


class Ticker(str, Enum):
    MATIC = "matic"
    BNB = "bnb"
    SOL = "sol"
    ETH = "eth"
    ETH2 = "eth2"
    BTC = "btc"
    ADA = "ada"


class InputDataType(str, Enum):
    HOLDINGS = "holdings"
    TXCOUNT = "txCount"
    TXFEE = "txFee"
    GASCONSUMPTION = "gasConsumption"


class AllocationStrategy(str, Enum):
    HOLDING = "holding-based"
    HYBRID = "hyrib-based"
    TXBASED = "tx-based"


class EntriesInRequest(BaseModel):
    date: str = Field(format="YYYY-MM-DD")
    value: int = Field(default=1)


class EntriesInResponse(BaseModel):
    date: str = Field(format="YYYY-MM-DD")
    entryValue: int = Field(default=1)
    outputValue: float


class MetaDataInResponse(BaseModel):
    sum: float
    mean: float
    inputUnit: InputUnit
    outputUnit: OutputUnit
    description: str
    warning: list = None


class EmissionQuery(BaseModel):
    ticker: Ticker
    inputDataType: InputDataType
    allocationStrategy: AllocationStrategy


class EmissionParams(BaseModel):
    inputUnit: InputUnit = Field(default="")
    outputUnit: OutputUnit = Field(default=OutputUnit.TON)
    predict: bool = Field(default=False)


class EmissionRequest(BaseModel):
    entries: list[EntriesInRequest]
    parameters: EmissionParams


class EmissionResponse(BaseModel):
    entries: list[EntriesInResponse] | None
    metaData: MetaDataInResponse | None
