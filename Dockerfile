FROM python:3.10-slim AS builder

RUN apt-get update && apt-get install -y build-essential && \
    pip install poetry==1.1.12

WORKDIR /carbon-emission

COPY pyproject.toml poetry.lock* /carbon-emission/

RUN poetry config virtualenvs.create false --local && \
    poetry install --no-dev --no-root --no-interaction --no-ansi

COPY app/ /carbon-emission/app/

FROM builder AS development

ENV APP_ENV=development
ENV APP_VERSION=0.1.0

COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]


FROM python:3.10-slim AS production

ENV APP_ENV=production

RUN useradd --uid 1000 --create-home devuser

WORKDIR /carbon-emission

COPY --from=builder /carbon-emission/pyproject.toml /carbon-emission/poetry.lock* /carbon-emission/
COPY --from=builder /carbon-emission/app/ /carbon-emission/app/

RUN pip install poetry==1.1.12

ARG APP_VERSION

ENV APP_VERSION=${APP_VERSION}

RUN poetry config virtualenvs.create false --local && \
    poetry install --no-dev --no-root --no-interaction --no-ansi && \
    chown -R devuser:devuser /carbon-emission && \
    chown -R devuser:devuser /usr/local

RUN chown -R devuser:devuser /carbon-emission

COPY entrypoint.sh /entrypoint.sh

RUN chown devuser:devuser /entrypoint.sh && \
    chmod +x /entrypoint.sh

USER devuser

ENTRYPOINT ["/entrypoint.sh"]
